import React from "react";
import { StyleSheet, Text, View, Image, TouchableOpacity, Linking } from "react-native";

export default function App() {
  return (
    <View style={styles.container}>
      <Image 
        style={{ width: 180, height: 180 }}
        source={require('./assets/anya.png')}
      />
      <Text style={styles.bigColor}>Spy, Mission, Waku Waku!</Text>
      <TouchableOpacity
          onPress={() => Linking.openURL("https://youtu.be/X3k0NPLsxgE")}
      >
          <Text>So Exciting!</Text>
      </TouchableOpacity>
    </View>
  );
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: '#F3CECE',
    alignItems: 'center',
    justifyContent: 'center',
  },
  bigColor: {
    fontSize: 18,
    textAlign: 'center',
    color: 'black',
  },
});